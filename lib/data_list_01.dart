import 'package:flutter/material.dart';

class MyApp02 extends StatelessWidget {
  const MyApp02({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("flutter demo"),
        ),
        body: HomeContent(),
      ),
    );
  }
}

class HomeContent extends StatelessWidget {
  HomeContent({Key? key}) : super(key: key);

  //动态列表数据：
  List listData = [
    {
      'title': '001',
      'img':
          "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/440px-Image_created_with_a_mobile_phone.png"
    },
    {
      'title': '002',
      'img':
          "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/TEIDE.JPG/2560px-TEIDE.JPG"
    },
    {
      'title': '003',
      'img':
          "https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Tourism_in_London795.jpg/1024px-Tourism_in_London795.jpg"
    },
  ];

  Widget _getListData(context, index) {
    //第二种设置数据：
    return Container(
      // constraints: BoxConstraints(
      //     maxWidth: 100, maxHeight: 100, minWidth: 30, minHeight: 40),
      child: Column(
        children: <Widget>[
          Image.network(listData[index]['img']),
          //SizedBox(height: 10),
          Text(
            listData[index]['title'],
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20),
          )
        ],
      ),
      decoration: BoxDecoration(
          border:
              Border.all(color: Color.fromRGBO(233, 233, 233, 0.9), width: 1)),
      margin: EdgeInsets.only(top: 10),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 10.0, //水平子Widget之间间距
          mainAxisSpacing: 10.0, //垂直子Widget之间间距
          crossAxisCount: 2 //一行的Widget数量
          ),
      itemCount: listData.length,
      itemBuilder: this._getListData,
    );
  }
}
