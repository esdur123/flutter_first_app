import 'package:flutter/material.dart';

class RowColumnAPP extends StatelessWidget {
  const RowColumnAPP({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('myTitle'),
        ),
        body: bodyColumnTest(),
      ),
    );
  }
}

class bodyColumnTest extends StatelessWidget {
  const bodyColumnTest({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center, //各圖高度不同才看得出來
      children: [unitColumn(), unitColumn(), unitColumn()],
    );
  }
}

class unitColumn extends StatelessWidget {
  const unitColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10,left: 5,right: 5),
      color: Colors.red,
      alignment: Alignment.center,
      constraints: BoxConstraints(maxWidth: 100, maxHeight: 100),
      child: Text(
        'ccc',
        style: TextStyle(fontSize: 36, color: Colors.amber),
      ),
    );
  }
}
