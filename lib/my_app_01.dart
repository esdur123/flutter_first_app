import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';

class MyApp01 extends StatelessWidget {
  const MyApp01({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text('無敵標題2'),
            foregroundColor: Colors.red,
            backgroundColor: Colors.yellow,
          ),
          body: Center(
              child: Container(
            alignment: Alignment.center,
            child: Text(
              '中間2！！！',
              style: TextStyle(fontSize: 20.0, color: Color(0xff0000ff)),
            ),
            //color: Color(0xfff7b3c2),
            constraints: BoxConstraints(
                maxWidth: 300, maxHeight: 400, minWidth: 30, minHeight: 40),
            margin: EdgeInsets.only(top: 50, left: 30),
            padding: EdgeInsets.only(right: 100, bottom: 10),
          )),
        ));
  }
}
